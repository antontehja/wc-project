class WC:
    f = None

    def __init__(self, namafile):
        self.at = namafile

    def word_count(self):
        self.f = open(self.at,'rt') 
        word = 0  
        for line in self.f : 
            word += len(line.split(" "))
        self.f.close()
        return word 

    def line_count(self):
        self.f = open(self.at,'rt') 
        lines = 0  
        for line in self.f : 
            lines += 1
        self.f.close()
        return lines

    def char_count(self):
        self.f = open(self.at,'rt') 
        char = 0  
        for line in self.f : 
            char += len(line)
        self.f.close()
        return char
